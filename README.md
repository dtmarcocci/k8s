# K8s
## K8s Local con Minikube

### Crear K8S con Minikube
```bash
minikube start \
--profile=cloud-computing \
--memory=2048 \
--cpus=2 \
--disk-size=5g \
--kubernetes-version=v1.24.6 \
--driver=docker \
--listen-address=0.0.0.0
```
### Cambiar al profile  cloud-computing
```bash
minikube profile cloud-computing
```
### Add plugins minikube
```bash
minikube addons enable ingress  ("minikube tunnel" and your ingress resources would be available at "127.0.0.1")
minikube addons enable metrics-server 
minikube addons enable helm-tiller 
```
### dashboard k8s
```bash
minikube dashboard --url
```

### Minikube comandos
[minikube docs](https://minikube.sigs.k8s.io/docs/handbook/accessing/)
```bash  
minikube ip
minikube tunnel --cleanup
```



# kubectl Comandos 

```bash
# ver nodos k8s
kubectl get nodes

# crear nuevo namespace
kubectl create namespace <NOMBRE-GRUPO>

# Cambio de namesapce
kubectl config set-context --current --namespace=<NOMBRE-GRUPO>

kubectl apply -f  deployment.yaml

kubectl apply -f  nginx-service.yaml

# Testing eliminar un POD dado un label selector (app=nginx)
kubectl delete pod --selector app=nginx

# Ejecutar un POD como si fuera Docker run . . .
# Utiliza nginx-service para acceder
kubectl run -i --rm --restart=Never curl-client --image=curlimages/curl --command -- curl -s 'http://nginx-service:80'

# Desplegar una quota
kubectl apply -f quota.yaml 

# Obtener datos de la resource quota dado un namespace
kubectl get resourcequota mem-cpu --namespace=<NOMBRE-GRUPO> --output=yaml

# Informacion de todos los servicios
kubectl get all

kubectl get svc
kubectl get pod
kubectl get pod --watch
kubectl get ingress
```

# Port-forward
```
kubectl port-forward service/mysql 33060:3306
```
Este comando de Kubernetes (k8s) significa que estás haciendo un reenvío de puerto (port-forward) del servicio (service) llamado "mysql" a tu máquina local. En este caso, estás redirigiendo el puerto 3306 del contenedor de MySQL (que es donde el servicio está ejecutándose) al puerto 33060 de tu máquina local.
Entonces, una vez que ejecutas este comando, puedes conectarte al servidor MySQL en el contenedor a través del puerto 33060 en tu máquina local. Esto puede ser útil, por ejemplo, si necesitas ejecutar consultas directamente en la base de datos para solucionar algún problema o realizar alguna tarea específica.

### Ingress local DNS
- sudo nano etc/hosts
- 127.0.0.1 nombregrupo.com

### Metrics
[prometheus-and-grafana](https://brain2life.hashnode.dev/prometheus-and-grafana-setup-in-minikube)
- kubectl create namespace metrics
- kubectl config set-context --current --namespace=metrics
- helm install prometheus prometheus-community/prometheus
- helm install grafana grafana/grafana
- kubectl get secret -n metrics grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
- minikube service grafana -n metrics --url

![img.png](image%2Fimg.png)
![img_2.png](image%2Fimg_2.png)
![img_1.png](image%2Fimg_1.png)
![img_3.png](image%2Fimg_3.png)

### Fin
- minikube delete --profile  cloud-computing


# K8s Cluster CloudComputing 

## agregar config local

- config a ./kube/config

### agregar a gitlab variable
- cat config | base64 > config.txt
- cat config.txt copiar a KUBE_CONFIG

#### CICI con k8s

- crear archivo .gitlab-ci.yml
```

stages:
  - deploy

deploy-k8s:
  stage: deploy
  image:
    name: bitnami/kubectl:latest
    entrypoint: [ "" ]
  before_script:
    - echo $KUBE_CONFIG | base64 -d > config
    - mv config /.kube/
  environment:
    name: cloudcomputing-$grupo
    url: http://k8s-lia.unrn.edu.ar/$grupo
    kubernetes:
      namespace: $grupo
  script:
    - kubectl config get-contexts
    - kubectl config use-context microk8s
    - kubectl config set-context --current --namespace=$grupo
    - kubectl get pods
    # hacer el deploy mediante kubectl 
  only:
    - main
```


# Certificados 

## agregar secret unrn-secret-tls con cert y key
```
kubectl create secret tls unrn-secret-tls --cert=tls.cert --key=tls.key
```

## modificaciones para agregar al ingress

```
 spec:
  tls:
    - hosts:
      - k8s-lia.unrn.edu.ar
      secretName: unrn-secret-tls
```

